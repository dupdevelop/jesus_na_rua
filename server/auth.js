var token = require('token');
const db = require('./db')
const _ = require('lodash')

token.defaults.secret = 'DJOE';
token.defaults.timeStep = 24 * 60 * 60; // 24h in seconds

var auth = {
    create: (name, pass) => {

        let token_auth;

        let mt = db.search('person', {
            name,
            pass
        })

        if (_.isUndefined(mt)) {
            // pegando o o id do usuario recem criado
            let id = db.insert('person', {
                name,
                pass
            })

            // gerando o token
            token_auth = token.generate(`${id}|person`)

            //fazendo a atualização com o token
            db.update('person', id, {
                token: token_auth
            })
        } else {
            token_auth = token.generate(`${mt.id}|person`)

            //fazendo a atualização com o token
            db.update('person', mt.id, {
                token: token_auth
            })
        }


        return db.search('person', {
            name,
            pass
        })
    },
    check: (json) => {
        console.log(json);
        return token.verify(json.id + '|person', json.token)
    }
}

module.exports = auth