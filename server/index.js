const app = require('express')();
const express = require('express');
const http = require('http').Server(app);
const bodyParser = require('body-parser')
const querystring = require('querystring');
const auth = require('./auth');
const db = require('./db')
var cors = require('cors')

/**
* -------------------------------
* middware
* -------------------------------
* **/
// parse application/json
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
    extended: false
}));

app.use(cors())

app.use(express.static('public'));

/**
* -------------------------------
* routes page
* -------------------------------
* **/
app.route('/')
.all(function (req, res, next) {
    // runs for all HTTP verbs first
    // think of it as route specific middleware!
    next();
})
.get(function (req, res, next) {
    res.sendFile(__dirname + "/public/index.html")
    //res.json(req.user);
})

/**
* -------------------------------
* routes API
* -------------------------------
* **/

app.route('/api/jnr/:table')
.all(function (req, res, next) {
    // runs for all HTTP verbs first
    // think of it as route specific middleware!

    next();
})
.get(function (req, res, next) {
    res.json({
        data: db.all(req.params.table)
    })
})
.post((req, res, next) => {
    req.body = JSON.parse(req.body.data)

    if (auth.check(req.body.auth))
        res.json({
            data: db.insert(req.params.table, req.body.data)
        })
    else
        res.json({
            type: "error",
            message: 'Auth necessario para a autenticação'
        })
})

app.route('/jnr/login')
.all(function (req, res, next) {
    // runs for all HTTP verbs first
    // think of it as route specific middleware!
    next();
})
.post((req, res, next) => {
    res.json({
        data: auth.create(req.body.name, req.body.pass)
    })
})


app.route('/jnr/user/create')
.all(function (req, res, next) {
    // runs for all HTTP verbs first
    // think of it as route specific middleware!
    next();
})
.post((req, res, next) => {
    res.json({
        data: auth.create(req.body.name, req.body.pass)
    })
})

//ouvindo os serviços
http.listen(process.env.PORT || 3004,
() => console.log('---------------------\n\nIm listen in port 3001\n\n-------------------'));
