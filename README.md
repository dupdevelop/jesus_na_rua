# stage-bootstrap-one-page-event-ticket-booking-theme
Stage is a stylish, one page theme for event ticket booking. It has a clean, minimal and modern design ideal for any type of event ticket booking. It is 100% mobile-friendly and built with Bootstrap.

# Modelo de comunicação entre cliente/servidor

Para a comunicação entre cliente e servidor é necessario que algumas coisas sema feitas no caso um objeto deve ser mandando
de um lado para o outro, segue abaixo o exemplo do objeto:

´´´
{
    data: JSON.stringify({
        data: {},
        auth: {}
    })
}
´´´
