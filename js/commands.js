var commands = [
    {
        term: 'version',
        category: 'info',
        action: function(){alert('CPC - 0.1Beta');}
    },
    {
        term: 'owner',
        category: 'info',
        action: function(){alert('Joerverson Santos \nhttps://github.com/joeverson');}
    },
    {
        term: 'horse',
        category: 'interarion',
        action: function(){$('body').prepend('<div class="fantasma"><audio controls autoplay="autoplay"><source src="http://www.w3schools.com/tags/horse.ogg" type="audio/ogg"><source src="http://www.w3schools.com/tags/horse.mp3" type="audio/mpeg">Your browser does not support the audio tag.</audio></div>'); $('.fantasma').css({opacity: '0', floar: 'left', position: 'absolute', top:'0', left: '0'});}
    },
    {
        term:'login',
        category: 'auth',
        action: function(){
            $('#modal-login').modal('show');
            app.modal.title = 'Fazer Login'            
        }
    },        
    {
        term:'sair',
        category: 'auth',
        action: function(){
            window.location.href=window.location.origin+"/logout";
        },
        description:"comando dado para você sair do sistema, fazer logout msm."
    },
    {
        term:'help',
        category: 'help',
        action: function(){
            var str = '<table class="table" border="1">';
            str += '<thead>';
            str += '<tr> <td>Termo</td><td>Categoria</td><td>Descrição</td></tr>';
            str += '</thead><tbody>';

            commands.forEach(function(a){
                str += "<tr> <td><b>"+a.term+"</b></td>  <td>"+a.category+"</td> <td>"+(a.description==undefined?'':a.description)+"</td></tr>";
            });

            str += '</tbody></table>';

            $('#myModalLabel').html('Helper do CPC <3');
            $('.modal-body').html(str);
            $('#myModal').modal('show');
            //alert(str);
        }
    }    
];
