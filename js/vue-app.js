$.fn.extend({
    animateCss: function (animationName, callback) {
        var animationEnd = (function (el) {
            var animations = {
                animation: 'animationend',
                OAnimation: 'oAnimationEnd',
                MozAnimation: 'mozAnimationEnd',
                WebkitAnimation: 'webkitAnimationEnd',
            };

            for (var t in animations) {
                if (el.style[t] !== undefined) {
                    return animations[t];
                }
            }
        })(document.createElement('div'));

        this.addClass('animated ' + animationName).one(animationEnd, function () {
            $(this).removeClass('animated ' + animationName);

            if (typeof callback === 'function') callback();
        });

        return this;
    },
});

/**
 *
 * ---------------------------------
 *    vue
 * ------------------------------------------
 * **/

let app = new Vue({
    el: '#app',
    mounted: function () {
        this.$nextTick(function () {
            $.get(app.$data.app.urlBase + "/api/jnr/notices", e => {
                app.$data.news.list = e.data
                console.log(e);
            })
        })
    },
    data: {
        app: {
            name: 'Jesus na Rua.',
            urlBase: 'http://192.168.1.8:3004',
            login: {
                auth: {},
                isLogged: false,
                name: '',
                pass: ''
            }
        },
        modal: {
            title: '',
            notice: {} // objeto de manipulação do notices
        },
        news: {
            list: [],
            newNotice: {
                title: '',
                date: date(),
                hour: hour(),
                text: ''
            }
        },
<<<<<<< HEAD

        local: {
          isMapped: false,
          list:[],



        },



=======
        movimento: {
            content: `
            Lorem ipsum dolor sit amet, ne accusam scripserit scribentur quo, omnis dolorum eum te. Ius tritani officiis recusabo eu,
                                    graecis vituperatoribus in duo. Vidit epicuri copiosae ex eam, et alienum consulatu vim.
                                    Ei eum ipsum persius, errem vidisse cu cum. Cu percipitur liberavisse per, nam dicunt
                                    blandit honestatis ut. Et dicta electram conclusionemque usu, per augue aeque at. No
                                    vix laoreet nominavi posidonium, ea pri malis ocurreret, exerci postulant mei ne. Per
                                    ut sensibus pericula, in regione feugiat vel, doctus commodo torquatos est ut. Eos populo
                                    possim omnesque cu. Agam convenire ei mel, qui at solum doming equidem. His equidem graecis
                                    ad, tritani eruditi pri ne. Ad graeci impedit dissentias per, ad choro bonorum consectetuer
                                    per, mel id scripta constituam reformidans. Ad vis iracundia scriptorem definitionem,
                                    mundi feugiat has ne, per sale dicunt regione ne. Ad cum commodo liberavisse, utamur
                                    fuisset est cu, te qui debet nominavi scribentur. Mea mundi legere fuisset ea, ex quo
                                    tale purto. Duo postulant petentium cu. Ne diceret voluptaria qui. Ei oblique volumus
                                    prodesset eam. Per eu atomorum dissentiet, labitur mediocrem constituam vix an. Ne mel
                                    semper discere definitionem, ne quem alienum mel. Vim id solet consul probatus. Cum vidisse
                                    expetenda corrumpit eu. Qui ridens vidisse in, accusam evertitur id usu. Vix suscipit
                                    ullamcorper at. Tibique deseruisse vix eu, essent meliore nominavi an vel, vim prompta
                                    laboramus prodesset ad.
            `
        },
        locais: {
            isMapped: false,
            list: [{
                name: 'Bancarios',
                hora: '18:00',
                dia: "terça",
                endereco: "Praça da Paz - Bancários, João Pessoa - PB",
                geo: {
                    lat: -7.1475762,
                    long: -34.8456343
                }
            }]
        },
        lideranca: {
            list: []
        },
>>>>>>> d6a07dc4767428832c9d11d92621863c5eec91d0
        slide: {
            current: {},
            storage: [{
                    image: "https://instagram.fjpa1-1.fna.fbcdn.net/vp/90de0122a73c9139dd4d39a5a45becdc/5B1700DA/t51.2885-15/e35/26871444_405011049933996_6810507049739223040_n.jpg",
                    title: "Jesus na \n Rua",
                    date: "20/02/2018 Ás 19:00 Hs"
                },
                {
                    image: "https://instagram.fjpa1-1.fna.fbcdn.net/vp/09d965caa7c9ccff2089a08d099fcb73/5B1BD5A9/t51.2885-15/e35/26068107_145540112774483_1180778842877329408_n.jpg",
                    title: "Ops",
                    date: "99/59/9999"
                },
                {
                    image: "https://instagram.fjpa1-1.fna.fbcdn.net/vp/bb3fd9940bff78c1e80bca0a6dd02f36/5B1BC318/t51.2885-15/s640x640/sh0.08/e35/24126358_559849751040820_8511239040106758144_n.jpg",
                    title: "novo essa foto vicc",
                    date: "99/59/9999"
                }
            ]
        }

    },
    methods: {
        open: (modal) => {
            app.$data.modal.title =_.upperFirst(modal)
            $('#modal-' + modal).modal('show')
        },
        random_slide: () => {

            // lista de imagens
            let it

            // decidindo que imagem vai colocar
            it = _.random(0, (app.$data.slide.storage.length - 1))

            //adicionando textos
            app.$data.slide.current = app.$data.slide.storage[it]


            //colocando a imagem de forma aleatoria
            document.getElementById('slides').style.background = "url(" + app.$data.slide.current.image + ") no-repeat center / cover"

            //animação
            $('#slides').animateCss('fadeIn')
        },
        listNews: () => {
            $.get(app.$data.app.urlBase + "/api/jnr/notices", {}, (e) => {
                console.log(e);
            })
        },
        login: () => {
            $.post(app.$data.app.urlBase + "/jnr/login", {
                name: app.$data.app.login.name,
                pass: app.$data.app.login.pass
            }, (e) => {

                if (!_.isEmpty(e.data)) {
                    app.$data.app.login.auth = e.data
                    app.$data.app.login.isLogged = true
                }

                $('#modal-login').modal('hide');
            })
        },
        //notices
        createNotice: () => {
            $.post(app.$data.app.urlBase + "/api/jnr/notices", {
                data: JSON.stringify({
                    data: app.$data.news.newNotice,
                    auth: app.$data.app.login.auth
                })
            }, (e) => {
                app.$data.news.list.push(app.$data.news.newNotice)

                app.$data.news.newNotice.title = ''
                app.$data.news.newNotice.text = ''
                console.log(e);
            })
        },
<<<<<<< HEAD
        map:(lat,lng) => {
          map = new google.maps.Map(document.getElementById('map'), {
         center: {lat , lng },
         zoom: 8

       })
     }
}

=======
        editNotice:(o)=>{
            app.$data.modal.notice = o
            $('#modal-edit-notice').modal('show')
        },
        //movimento TODO testar esse methodo
        editMovimento: () => {
            $.put(app.$data.app.urlBase + "/api/jnr/movimento", {
                data: JSON.stringify({
                    data: app.$data.movimento,
                    auth: app.$data.app.login.auth
                })
            }, (e) => {
                app.$data.news.movimento = e.data.movimento
                console.log(e);
            })
        },
        maps: (o) => {

            app.$data.locais.isMapped = !app.$data.locais.isMapped


            if (app.$data.locais.isMapped) {
                var uluru = {
                    lat: o.geo.lat,
                    lng: o.geo.long
                };

                console.log(document.getElementById("map"));


                var map = new google.maps.Map(document.getElementById("map"), {
                    zoom: 14,
                    center: uluru,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                });


                var marker = new google.maps.Marker({
                    position: uluru,
                    map: map
                });
            }


        }
    }
>>>>>>> d6a07dc4767428832c9d11d92621863c5eec91d0
});



//slide automatico
setInterval(function () {
<<<<<<< HEAD
    app.random_slide()
}, 5000);
=======

    if (!app.$data.locais.isMapped) {
        app.random_slide()
    }
}, 5000);


function date() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();

    if (dd < 10) {
        dd = '0' + dd
    }

    if (mm < 10) {
        mm = '0' + mm
    }

    return yyyy + "-" + mm + "-" + dd
}

function hour() {
    var today = new Date();
    var h = today.getHours();
    var m = today.getMinutes()
    var s = today.getSeconds();

    if (h < 10) {
        h = '0' + h
    }

    if (m < 10) {
        m = '0' + m
    }

    return h + ':' + m + ':' + s;
}
>>>>>>> d6a07dc4767428832c9d11d92621863c5eec91d0
